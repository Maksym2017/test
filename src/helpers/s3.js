const S3 = require('aws-sdk/clients/s3');
const AWS = require('aws-sdk');
const s3Endpoint = new AWS.Endpoint(process.env.S3_ENDPOINT);
const imageResize = require('../helpers/imageResize')

class Storage {
    constructor() {
        let accessKeyId = process.env.S3_ACCESS_KEY;
        let secretAccessKey = process.env.S3_SECRET_KEY;
        this.s3 = new S3({
            endpoint: s3Endpoint,
            region: process.env.S3_REGION,
            accessKeyId,
            secretAccessKey,
            signatureVersion: process.env.S3_SIGNATURE
        });


    }

    async putObject(key, req) {

        return this.s3.putObject({
                Body: req,
                Bucket: process.env.S3_BUCKET,
                Key: key,
                ContentType: req.headers['content-type'],
                ContentLength: req.headers['content-length']
            }
            , (err, data) => {
                if (err) {
                    console.log(err + 'ERR');
                }
            }).promise();
    }

    async putImagesObject(key, image) {

        return this.s3.putObject({
                Body: await image.toBuffer(),
                Bucket: process.env.S3_BUCKET,
                Key: key,
            }
            , (err, data) => {
                if (err) {
                    console.log(err + 'ERR');
                }
            }).promise();
    }

    async putLargeImg(key, req) {
        return this.putImagesObject('large-' + key, req.pipe(await imageResize.resizeLarge()))
    }

    async putMediumImg(key, req) {
        return this.putImagesObject('medium-' + key, req.pipe(await imageResize.resizeMedium()))
    }

    async putThumbImg(key, req) {
        return this.putImagesObject('thumb-' + key, req.pipe(await imageResize.resizeThumb()))
    }

    async putImagePacket(key, req) {

        return Promise.all([
            this.putLargeImg(key, req),
            this.putMediumImg(key, req),
            this.putThumbImg(key, req)
        ])
    }
}

const s3 = new Storage();
module.exports = s3;
