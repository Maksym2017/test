const sharp = require('sharp');

class ImageResize {

    async resizeLarge(){

        return sharp()
            .resize(parseInt(process.env.LARGE_HEIGHT), parseInt(process.env.LARGE_WIDTH),{fit:'contain'})

    }

    async resizeMedium(){
        return sharp()
            .resize(parseInt(process.env.MEDIUM_HEIGHT), parseInt(process.env.MEDIUM_WIDTH),{fit:'contain'})

    }

    async resizeThumb(){

        return sharp()
            .resize(parseInt(process.env.THUMB_HEIGHT), parseInt(process.env.THUMB_WIDTH),{fit:'contain'})
    }

}

const imageResize = new ImageResize()

module.exports = imageResize