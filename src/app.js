require('dotenv').config()
const express = require('express')
const app = express()
const s3 = require('./helpers/s3.js')
const checkType = require('./middleware/checkType')

app.put('/:filename', checkType, async (req, res) => {
    let filename = req.params.filename

    if (req.docsSend) {
        await s3.putObject(filename, req)
    } else {
        await s3.putImagePacket(filename, req)
    }

    res.sendStatus(200)
})

app.listen(process.env.APP_PORT)