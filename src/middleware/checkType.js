module.exports = (req, res, next) => {
    let contentType = req.headers['content-type']
    let contentLength = req.headers['content-length']

    if (typeof contentType === "undefined" || typeof contentLength === "undefined") {
        res.sendStatus(403)
        return;
    }

    if (contentLength > parseInt(process.env.MAX_CONTENT_LENGTH)) {
        res.sendStatus(403)
        return;
    }


    let acceptedTypes = process.env.ACCEPTED_TYPE.split(';')

    if (acceptedTypes.indexOf(contentType) === -1) {
        res.sendStatus(403)
        return;
    }

    let acceptedExpansion = process.env.ACCEPTED_EXPANSION.split(';')
    let reqExpansion = req.params.filename.split('.').pop()

    if (acceptedExpansion.indexOf(reqExpansion) === -1) {
        res.sendStatus(403)
        return;
    }

    req.docsSend = false

    if (contentType.indexOf('image') === -1) {
        req.docsSend = true
    }

    next()
}